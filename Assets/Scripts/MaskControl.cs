﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskControl : MonoBehaviour {

    public RectTransform mask;
    public RectTransform maskedObjects;    
    
    public Condition condition;
    public Text conditionText;

    Vector3 offset = Vector3.zero;
    Transform virtualMask;
    Vector3 startPos;    
    Vector4 mouseArea = Vector4.zero;
    bool isDragging = false;
    bool pinFrame { get { return (condition == Condition.FIXED_ABS || condition == Condition.FIXED_REL); } }
    bool useAbsolutePosition { get { return (condition == Condition.FIXED_ABS || condition == Condition.MOVING_ABS); } }
    bool restrictMouse { get { return false; } } // to restrict, add condition check for conditions that apply, like above.
    
    void Start()
    {
        startPos = mask.position;

        // Get current mask area in case we want to restrict scrolling inside it.
        float x = mask.rect.width / 2.0f;
        float y = mask.rect.height / 2.0f;
        mouseArea = new Vector4(mask.position.x - x, mask.position.x + x, mask.position.y - y, mask.position.y + y);

        // Create virtual mask to make calculating our mask translation easier
        if (virtualMask == null) {
            virtualMask = new GameObject("VirtualMask").transform;
            virtualMask.parent = mask.parent;
        }

        SetCondition((int)condition);
    }

    public void SetCondition(float i)
    {
        condition = (Condition)(int)i;
        isDragging = false;

        mask.position = startPos;
        virtualMask.position = startPos;
        maskedObjects.localPosition = -mask.localPosition;

        conditionText.text = condition.ToString();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
            return;
        }

        Vector3 mPos = Input.mousePosition;

        if (Input.GetMouseButtonDown(0) && IsMouseValid()) {
            // Get mouse pointer position so we can move relative to that point.
            if (pinFrame) {
                offset = (useAbsolutePosition) ? mPos - virtualMask.position : mPos - maskedObjects.localPosition;
            } else {
                offset = (useAbsolutePosition) ? mPos - mask.position : mPos - virtualMask.position;
            }
            isDragging = true;
        } 
        if (Input.GetMouseButtonUp(0) || !IsMouseValid()) {
            isDragging = false;
        }

        // Drag mask or content, depending on condition
        if (isDragging) {
            Vector3 newPos = mPos - offset;
            if (pinFrame) {
                if (useAbsolutePosition) {
                    virtualMask.position = FitScreen(newPos);
                    maskedObjects.localPosition = -virtualMask.localPosition;
                } else {
                    maskedObjects.localPosition = FitScreen(newPos);
                }                                            
            } else {
                if (useAbsolutePosition) {
                    mask.position = FitScreen(newPos);
                } else {
                    virtualMask.position = FitScreen(newPos);
                    mask.localPosition = -virtualMask.localPosition;
                }                
            }          
        }
    }

    // Counteract frame movement to make masked content appear stable in unpinned condition.
    void LateUpdate()
    {
        if (isDragging && !pinFrame) {            
            maskedObjects.localPosition = -mask.localPosition;
        }        
	}

    // Check if mouse is within restricted area, if we care about that at all
    bool IsMouseValid()
    {                
        if (!restrictMouse) {
            return true;
        }
        Vector3 p = Input.mousePosition;
        return (p.x > mouseArea[0] && p.x < mouseArea[1] && p.y > mouseArea[2] && p.y < mouseArea[3]);
    }

    // Constrain all movement to the confines of the screen.
    Vector3 FitScreen(Vector3 position)
    {
        Vector3 p = position;
        if (pinFrame && !useAbsolutePosition) {
            float x = (Screen.width - mask.rect.width) / 2.0f;
            float y = (Screen.height - mask.rect.height) / 2.0f;
            p.x = Mathf.Clamp(p.x, -x, x);
            p.y = Mathf.Clamp(p.y, -y, y);   
        } else {
            float x = mask.rect.width / 2.0f;
            float y = mask.rect.height / 2.0f;
            p.x = Mathf.Clamp(p.x, x, Screen.width - x);
            p.y = Mathf.Clamp(p.y, y, Screen.height - y);
        }
        return p;
    }
}

public enum Condition
{
    FIXED_REL,
    FIXED_ABS,
    MOVING_REL,
    MOVING_ABS
}
